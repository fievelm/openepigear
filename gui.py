from tkinter import *
from tkinter import ttk
import hashlib
import tkinter.scrolledtext as tkst
from OpenEpiGear import EpicorDatabase


__version__ = '0.1.1'

version_info = 'OpenEpiGear GUI v. ' + __version__

'''

What do we want to do?

Test

Startup / Shutdown App & DB

Backup / Restore


'''

def getcoords(xypos):
    regx='.*?\\d+.*?\\d+.*?(\\d+).*?(\\d+)'
    rg = re.compile(regx,re.IGNORECASE|re.DOTALL)
    m = rg.search(xypos)
    if m:
        int1=m.group(1)
        int2=m.group(2)
    return int(int1), int(int2)


class MainWindow:
    def __init__(self, master):
        s = ttk.Style()
        s.theme_use('classic')
        '''
        s.configure('OEG.TFrame', background='light sea green')
        s.configure('OEG.TLabel', background='black', foreground='misty rose', font=("Helvetica", 16))
        s.configure('OEGblank.TLabel', background='light sea green', foreground='light sea green')
        s.configure('OEG.TRadiobutton', background='light sea green')
        s.configure('Ro.TFrame', background='red', foreground='red')
        s.configure('OEGWild.TButton', cursor='boat')
        s.map('OEGWild.TButton',
              foreground=[('disabled', 'light grey'),
                          ('pressed', 'black'),
                          ('active', 'blue')],
              background=[('disabled', 'grey'),
                          ('pressed', 'yellow'),
                          ('active', 'misty rose')],
              relief=[('pressed', 'groove'),
                      ('!pressed', 'ridge')])

        s.map('OEG.TRadiobutton',
              background=[('active', 'green')])
        '''
        self.backcolor='grey'
        self.password = None
        self.username = None
        self.whichdb = StringVar()

        self.master = master
        self.master.configure(bg=self.backcolor)
        self.frame = ttk.Frame(self.master, style='OEG.TFrame')
        self.bottomframe = ttk.Frame(self.master, style='OEG.TFrame')
        self.testframe = ttk.Frame(self.master, style='OEG.TFrame')
        self.shutdownframe = ttk.Frame(self.master)
        self.startupframe = ttk.Frame(self.master)
        self.analframe = ttk.Frame(self.master, style='OEG.TFrame')
        self.backupframe = ttk.Frame(self.master, style='OEG.TFrame')
        self.restoreframe = ttk.Frame(self.master, style='OEG.TFrame')

        self.frame.grid(row=0, column=0, padx=5, pady=5, sticky='nsew', rowspan=2)
        self.testframe.grid(row=0, column=1, padx=5, pady=5, sticky='ns')
        self.shutdownframe.grid(row=0, column=2, padx=5, pady=5, sticky='ns')
        self.bottomframe.grid(row=2, column=0, rowspan=2, padx=5, pady=5, sticky='s')
        self.startupframe.grid(row=1, column=2, padx=5, pady=5, sticky='ns')
        self.analframe.grid(row=1, column=1, padx=5, pady=5, sticky='ns')
        self.backupframe.grid(row=2, column=1, padx=5, pady=5, sticky='ew')
        self.restoreframe.grid(row=2, column=2, padx=5, pady=5, sticky='ew')
        self.thing = []
        self.menubar = Menu(master)
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label='OEG Settings', command=self.ini_editor)
        self.filemenu.add_command(label='About', command=self.aboutwindow)
        self.filemenu.add_separator()
        self.filemenu.add_command(label='Exit', command=self.exitapp)
        self.menubar.add_cascade(label='File', menu=self.filemenu)
        self.menubar.add_command(label="Test", command=self.test)
        master.config(menu=self.menubar)

        self.whichdb.set("DEBUG")
        self.DatabaseLabel = ttk.Label(self.frame, text='Database', style="OEG.TLabel", width=15).grid(row=0, column=0)
        #self.frame.grid_rowconfigure(1, weight=1)
        self.placeholder = ttk.Label(self.frame, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.radioselect = ttk.Radiobutton(self.frame, text='  Debug  ', variable=self.whichdb, value="DEBUG",
                                        width=15, style='OEG.TRadiobutton', command=lambda: self.live_mode('off')).grid(row=2, column=0)
        self.radiolive = ttk.Radiobutton(self.frame, text='  Live  ', variable=self.whichdb, value="EpicorLive",
                                        width=15, style='OEG.TRadiobutton', command=lambda: self.live_mode('on')).grid(row=3, column=0)
        self.radiopilot = ttk.Radiobutton(self.frame, text='  Pilot  ', variable=self.whichdb, value="EpicorPilot",
                                         width=15, style='OEG.TRadiobutton', command=lambda: self.live_mode('off')).grid(row=4, column=0)
        self.radiotest = ttk.Radiobutton(self.frame, text='  Test  ', variable=self.whichdb, value="EpicorTest",
                                        width=15, style='OEG.TRadiobutton', command=lambda: self.live_mode('off')).grid(row=5, column=0)
        self.radiottrain = ttk.Radiobutton(self.frame, text='  Train  ', variable=self.whichdb, value="EpicorTrain",
                                        width=15, style='OEG.TRadiobutton', command=lambda: self.live_mode('off')).grid(row=6, column=0)


        self.bottomlabel = Label(self.bottomframe, text=version_info)
        self.bottomlabel.grid(row=4, column=1, sticky='s')


        self.TestConnectLabel = ttk.Label(self.testframe, text='Tests', width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.testframe).grid(row=1, column=0) #blank space
        self.TestConButton = ttk.Button(self.testframe, text='ODBC Check', width=15, command=self.TestConnection).grid(row=2, column=0)
        self.TestPaths = ttk.Button(self.testframe, text='Path Check', style='clam.TButton', width=15, command=self.TestPaths).grid(row=3, column=0)
        self.placeholder = ttk.Label(self.testframe, style='OEGblank.TLabel').grid(row=4, column=0) #blank space

        self.ShutDownLabel = ttk.Label(self.shutdownframe, text='Shutdown', style="OEG.TLabel", width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.shutdownframe, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.SD_AppServerBtn = ttk.Button(self.shutdownframe, text='AppServers', style='OEGWild.TButton', width=15, command=self.SD_AppServer).grid(row=2, column=0)
        self.SD_DatabaseBtn = ttk.Button(self.shutdownframe, text='Database', style='OEGWild.TButton', width=15, command=self.SD_Database).grid(row=3, column=0)

        self.StartupLabel = ttk.Label(self.startupframe, text='Startup', style="OEG.TLabel", width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.startupframe, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.SU_AppServerBtn = ttk.Button(self.startupframe, text='AppServers', style='OEGWild.TButton', width=15, command=self.SU_AppServer).grid(row=2, column=0)
        self.SU_DatabaseBtn = ttk.Button(self.startupframe, text='Database', style='OEGWild.TButton', width=15, command=self.SU_Database).grid(row=3, column=0)
        self.placeholder = ttk.Label(self.startupframe, style='OEGblank.TLabel').grid(row=4, column=0) #blank space

        self.AnalLabel = ttk.Label(self.analframe, text='Analysis', style="OEG.TLabel", width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.analframe, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.AS_TabAnal = ttk.Button(self.analframe, text='Table Analysis', style='OEGWild.TButton', width=15, command=self.TabAnalys).grid(row=2, column=0)
        self.AS_SqlQ = ttk.Button(self.analframe, text='SQL Query', style='OEGWild.TButton', width=15, command=self.Sqlquery).grid(row=3, column=0)

        self.BackupLabel = ttk.Label(self.backupframe, text='Backup', style="OEG.TLabel", width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.backupframe, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.BU_OnlineBackup = ttk.Button(self.backupframe, text='Online Backup', style='OEGWild.TButton', width=15, command=self.OnlineBackup).grid(row=2, column=0, sticky='nsew')
        self.placeholder = ttk.Label(self.backupframe, style='OEGblank.TLabel').grid(row=3, column=0) #blank space

        self.RestoreLabel = ttk.Label(self.restoreframe, text='Restore', style="OEG.TLabel", width=15).grid(row=0, column=0)
        self.placeholder = ttk.Label(self.restoreframe, style='OEGblank.TLabel').grid(row=1, column=0) #blank space
        self.RestoreDB_Btn = ttk.Button(self.restoreframe, text='Restore Backup', style='OEGWild.TButton', width=15, command=self.OnlineBackup).grid(row=2, column=0, sticky='nsew')
        self.placeholder = ttk.Label(self.restoreframe, style='OEGblank.TLabel').grid(row=3, column=0) #blank space

        self.frame.bind_all('<Key>', self.keyhandler)


    def new_window(self):
        self.newWindow = Toplevel(self.master)
        self.app = SubWindow(self.newWindow)

    def aboutwindow(self):
        self.newWindow = Toplevel(self.master)
        self.master.wait_window(AboutWindow(self.newWindow))
        #self.app = AboutWindow(self.newWindow)

    def live_mode(self, onoff):
        if onoff == 'on':
            self.master.configure(bg='yellow')
            self.bottomlabel.configure(text='WARNING: LIVE MODE', bg='red')
        elif onoff == 'off':
            self.master.configure(bg=self.backcolor)
            self.bottomlabel.configure(text=version_info, bg='light grey')
        else:
            raise RuntimeError('mode invalid')
        return



    def GetPassword(self):
        self.pwwindow = Toplevel(self.master)
        self.tApp = PasswordDialog(self.pwwindow,self)
        self.master.wait_window(self.pwwindow)



    '''
        Button Commands
    '''

    def test(self):
        pass



    def TestConnection(self):
        '''
            Test ODBC Link
        '''
        self.GetPassword()
        self.testWindow = Toplevel(self.master)
        self.tApp = MessageWindow(self.testWindow)
        self.tApp.textbox.insert(INSERT, "Username: " + self.username + "\n")
        if (self.whichdb.get() == "DEBUG"):
            self.tApp.textbox.insert(INSERT, "No Database Selected, in DEBUG mode.\n")
            return
        self.tApp.textbox.insert(INSERT, "Opening " + self.whichdb.get() + " configuration.\n")
        try:
            db = EpicorDatabase(self.whichdb.get())
        except RuntimeError as e:
            self.tApp.textbox.insert(INSERT, "ERROR: \n" + str(e.args))
            return
        self.tApp.textbox.insert(INSERT, "Connecting to " + self.whichdb.get() + " ODBC.\n")
        try:
            results = db.Connect(self.username, self.password)
        except:
            self.tApp.textbox.insert(INSERT, "Connect Failure!\n")
        try:
            db.TestConnection()
            self.tApp.textbox.insert(INSERT, "Connected Successfully!\n")
        except:
            self.tApp.textbox.insert(INSERT, "Cursor failure!.\nResults: " + results)

    def keyhandler(self, event):
        self.thing.append(event.char)
        if (hashlib.sha256(str(''.join(self.thing[-7:])).encode('utf-8')).hexdigest()[:8] == 'a5ce2bb2'):
            x=0; z=''
            for idx, val in enumerate([77, 19, 4, -1, -10, -71]):
                x += val + idx; z += chr(x)
            MessageWindow(Toplevel(self.master), str(z))

    def TestPaths(self):
        self.testWindow = Toplevel(self.master)
        self.tApp = MessageWindow(self.testWindow)
        self.tApp.frame.focus()
        if (self.whichdb.get() == "DEBUG"):
            self.tApp.textbox.insert(INSERT, "No Database Selected, in DEBUG mode.\n")
            return
        self.tApp.textbox.insert(INSERT, "Testing oeg.ini params.\n")
        try:
            db = EpicorDatabase(self.whichdb.get())
            results = db.VerifyConfig()
            self.tApp.textbox.insert(INSERT, results)
        except:
            self.tApp.textbox.insert(INSERT, "General error. Database name non-existant in oeg.ini")
        finally:
            self.tApp.frame.focus()

    def SD_AppServer(self):
        '''
        SHUTDOWN appserver
        '''
        pass

    def SD_Database(self):
        '''
        SHUTDOWN database
        '''
        pass

    def SU_AppServer(self):
        pass

    def SU_Database(self):
        pass

    def TabAnalys(self):
        pass

    def OnlineBackup(self):
        pass

    def Sqlquery(self):
        pass

    def VerifySelectedConfig(self):
        db = EpicorDatabase(self.whichdb.get())
        if (db.VerifyConfig() == True):
            return
        else:
            MessageWindow("Config Error!")

    def ini_editor(self):
        '''
        Manually edit oeg.ini file.
        '''
        def save_changes():
            '''
            There's probably a special hell for doing this...
            '''
            newcontents = open('OEG.ini', 'w')
            # remove last character, as it adds a return line on edit...
            newcontents.write(self.tApp.editor.get('1.0', END + '-1c'))
            newcontents.close()

        self.iniwindow = Toplevel(self.master)
        self.tApp = SubWindow(self.iniwindow)
        self.tApp.subframe = Frame(self.tApp.frame)

        self.tApp.editor = tkst.ScrolledText(self.tApp.subframe, width=100, height=35, wrap='none')
        contents = open('OEG.ini', 'r')
        self.tApp.editor.insert('1.0', contents.read())
        contents.close()
        self.tApp.editor.pack()
        self.tApp.savebtn = Button(self.tApp.frame, text='Save', width=25, command=save_changes)
        self.tApp.cancelbtn = Button(self.tApp.frame, text='Cancel', width=25, command=self.tApp.close_windows)
        self.tApp.subframe.pack()
        self.tApp.savebtn.pack()
        self.tApp.cancelbtn.pack()

        self.tApp.frame.pack()


    def exitapp(self):
        self.master.destroy()
        pass


class SubWindow(object):
    '''
    Base window
    '''
    def __init__(self, parent):
        self.master = parent
        self.frame = Frame(self.master)
        x, y = getcoords(self.master.master.geometry())
        x += 20
        y += 20
        self.master.geometry('+' + str(x) + '+' + str(y))
        self.frame.focus()

    def close_windows(self):
        self.master.destroy()


class PasswordDialog(SubWindow, object):
    def __init__(self, parent, rootwin):
        SubWindow.__init__(self, parent)

        self.rootwin = rootwin
        self.lab = Label(self.frame, text='ODBC Login:')
        self.lab.pack()
        self.usern = Entry(self.frame)
        self.usern.pack()
        self.entry = Entry(self.frame, show='*')
        self.entry.bind("<KeyRelease-Return>", self.StorePassEvent)
        self.entry.pack()
        if (self.rootwin.username and self.rootwin.password):
            self.usern.insert(0, self.rootwin.username)
            self.entry.insert(0, self.rootwin.password)
        self.usern.focus()
        self.button = Button(self.frame)
        self.button["text"] = "Submit"
        self.button["command"] = self.StorePass
        self.button.pack()
        self.frame.pack()

    def StorePassEvent(self, event):
        self.StorePass()

    def StorePass(self):
        self.rootwin.password = self.entry.get()
        self.rootwin.username = self.usern.get()
        self.close_windows()




class MessageWindow(SubWindow):
    '''
    Base window with textbox & quit button
    '''
    def __init__(self, parent, msg=None):
        SubWindow.__init__(self, parent)
        self.textbox = Text(self.frame)
        self.textbox.pack()
        if (msg != None):
            self.textbox.insert(INSERT, msg)
        self.quitButton = Button(self.frame, text='OK', width=25, command=self.close_windows)
        self.quitButton.pack()
        self.frame.pack()


class AboutWindow(MessageWindow):
    def __init__(self, parent):
        MessageWindow.__init__(self, parent)
        self.textbox.configure(width=40, height=10, wrap='word')
        self.textbox.insert(INSERT, 'About box stuff. Here is where we put copyright and whatnot.')
        self.quitButton.configure(text='OK Thanks!')
        self.frame.pack()

    def close_windows(self):
        self.master.destroy()




def main():
    root = Tk()
    root.title("OpenEpiGear Gooey")
    app = MainWindow(root)
    root.mainloop()

if __name__ == '__main__':
    main()
