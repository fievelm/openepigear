#!/usr/bin/env python
# server.py
__version__ = "0.1"
import socket
import select
import queue
from threading import Thread
from time import sleep
from random import randint
import sys
import pickle

import OpenEpiGear

CFG_PORT = 1030


class ProcessThread(Thread):
    def __init__(self):
        super(ProcessThread, self).__init__()
        self.running = True
        self.q = queue.Queue()

    def add(self, data):
        self.q.put(data)

    def stop(self):
        self.running = False

    def run(self):
        q = self.q
        while self.running:
            try:
                # block for 1 second only:
                value = q.get(block=True, timeout=1)
                process(value)
            except queue.Empty:
                sys.stdout.write('.')
                sys.stdout.flush()
        #
        if not q.empty():
            print("Elements left in the queue:")
            while not q.empty():
                print(q.get())

t = ProcessThread()
t.start()


def process(value):
    """



    Command Module



    """

    x = pickle.loads(value)

    # ETS and ETC must be same.
    if (x.get("version") != __version__):
        print("Wrong version!")
        return
    if(x.get("COMMAND") == "Say Hello"):
        print("Hello there!")
    elif(x.get("COMMAND") == "Connect"):
        print("Connecting to DB...")
    elif(x.get("COMMAND") == "TestSQL"):
        p = OpenEpiGear.EpicorDatabase("EpicorTest")
        r, cols, table = p.Sql(x.get("arg1"))
        #p.Commit()
        print ('\n'.join(cols))
        print ('\n'.join(table))
        p.Close()
    else:
        print("I do not understand.")

    sleep(randint(1, 9))    # emulating processing time


def main():
    s = socket.socket()          # Create a socket object
    host = socket.gethostname()  # Get local machine name
    port = CFG_PORT              # Reserve a port for your service.
    s.bind((host, port))         # Bind to the port
    print("Listening on port {p}...".format(p=port))

    s.listen(5)                 # Now wait for client connection.
    while True:
        try:
            client, addr = s.accept()
            ready = select.select([client, ], [], [], 2)
            if ready[0]:
                data = client.recv(4096)
                #print data
                t.add(data)
        except KeyboardInterrupt:
            print
            print("Stop.")
            break
        except socket.error as msg:
            print("Socket error! %s", msg)
            break
    #
    cleanup()


def cleanup():
    t.stop()
    t.join()

#########################################################

if __name__ == "__main__":
    main()