from getpass import getpass
from OpenEpiGear import EpicorDatabase


p = EpicorDatabase("EpicorPilot")
p.Connect(input("ODBC Username:"), getpass('ODBC Password:'))

if p.TestConnection():
    print('Connected!')
else:
    print('Not Connected! Are your username & password correct?')
    raise SystemExit

print(p.VerifyConfig())  # Verify paths & ports are correct

input("\nPress Enter to Continue...\n")
r, cols, table = p.Sql('select partnum, ium from pub.part where inactive=1')  # We'll run a SQL query now:
print(cols)  # Column headers
print('\n'.join(table))  # Results

p.Close()


from random import getrandbits


def maybe():
    return getrandbits(1)
