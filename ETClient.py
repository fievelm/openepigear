import sys
import os
import socket
import pickle

__version__ = "0.1"


if (os.path.isfile('correcthorse')):
    with open('correcthorse') as batterystaple:
        TMPPW = batterystaple.readline()
else:
    TMPPW = ''

cmds = dict(COMMAND="testconnect", version=__version__, user="jeffj", password=TMPPW, arg1="SELECT COMPANY")
#dump to string.
z = pickle.dumps(cmds)


def main():
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = socket.gethostname()
        client.connect((host, 1030))
        #client.send(b"Hi there!")
        client.send(z)
        client.shutdown(socket.SHUT_RDWR)
        client.close()
    except Exception as msg:
        print(msg)

#########################################################

main()
